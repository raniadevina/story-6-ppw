from django.contrib import admin
from django.urls import path
from features import views
app_name = 'features'

urlpatterns = [
    path('gallery/',views.gallery, name='gallery'),
    path('favorites/',views.favorites, name='favorites'),
    path('schedule/',views.schedule,name = 'schedule'),
    path('delete/<int:delete_id>/',views.delete, name='delete'),
    path('data/',views.data,name = 'data'),
    path('',views.index,name='Story5'),
]
