from django.http import HttpResponse
from django.shortcuts import render ,redirect
from features import forms
from .models import MataKuliah


def index(request):
    return render(request,"index.html")

def data(request):
    listMatkul = MataKuliah.objects.all().values()
    return render(request,'data.html',{'data':listMatkul})

def favorites(request):
    return render(request,"favorites.html")

def gallery(request):
    return render(request,"gallery.html")

def schedule(request):
    form_matkul = forms.FormMatkul()
    if request.method == 'POST':
        form_matkul_input = forms.FormMatkul(request.POST or None)
        if form_matkul_input.is_valid():
            databersih = form_matkul_input.cleaned_data
            matkul_input = MataKuliah()
            matkul_input.nama_matkul = databersih['nama_matkul']
            matkul_input.jumlah_sks = databersih['jumlah_sks']
            matkul_input.ruangan = databersih['ruangan']
            matkul_input.dosen = databersih['dosen']
            matkul_input.semester = databersih['semester']
            matkul_input.deskripsi = databersih['deskripsi']
            matkul_input.save()
            current_data = MataKuliah.objects.all()
            return redirect('/data')  
        else:
            current_data = MataKuliah.objects.all()
            return render(request, 'schedule.html',{'form':form_matkul, 'status':'failed','databersih':current_data})
    else:
        current_data = MataKuliah.objects.all()
        return render(request, 'schedule.html',{'form':form_matkul,'databersih':current_data})



def delete(request, delete_id):
    try:
        jadwal_to_delete = MataKuliah.objects.get(pk = delete_id)
        jadwal_to_delete.delete()
        return redirect('/data')
    except:
        return redirect('/data')
