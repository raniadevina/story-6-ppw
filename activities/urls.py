from django.contrib import admin
from django.urls import path
from . import views
app_name = 'activities'

urlpatterns = [
    path('activity/',views.activity,name='activity'),
    path('listActivity/', views.listActivity, name='listActivity'),
    path('daftar/<int:task_id>/', views.daftar, name='daftar'),
    path('delete/<int:delete_id>/', views.delete, name='delete'),
    path('activities/', views.index, name='Story6'),
]

