from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .models import Kegiatan, Orang
from .forms import FormKegiatan, FormOrang
from .views import index, activity, listActivity, daftar, delete
from django.apps import apps
from .apps import ActivitiesConfig

# Create your tests here.

class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(nama_kegiatan="masak",deskripsi_kegiatan ="membuat sandwich")
        self.orang = Orang.objects.create(nama_orang="RaniaDevina")

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(),1)
        self.assertEqual(Orang.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan),"masak")
        self.assertEqual(str(self.orang), "RaniaDevina")

class FormTest(TestCase):

    def test_form_is_valid(self):
        form_kegiatan = FormKegiatan(data={
            "nama_kegiatan": "membaca",
            "deskripsi_kegiatan": "membaca buku"
        })
        self.assertTrue(form_kegiatan.is_valid())
        form_orang = FormOrang(data={
            'nama_orang': "Rania"
        })
        self.assertTrue(form_orang.is_valid())

    def test_form_invalid(self):
        form_kegiatan = FormKegiatan(data={})
        self.assertFalse(form_kegiatan.is_valid())
        form_orang = FormOrang(data={})
        self.assertFalse(form_orang.is_valid())


class UrlsTest(TestCase):

    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="belajar", deskripsi_kegiatan="belajar PPW")
        self.orang = Orang.objects.create(
            nama_orang="RaniaDevina", kegiatan=Kegiatan.objects.get(nama_kegiatan="belajar"))
        self.index = reverse("activities:Story6")
        self.listActivity = reverse("activities:listActivity")
        self.activity = reverse("activities:activity")
        self.daftar = reverse("activities:daftar", args=[self.kegiatan.pk])
        self.delete = reverse("activities:delete", args=[self.orang.pk])

    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

    def test_listActivity_use_right_function(self):
        found = resolve(self.listActivity)
        self.assertEqual(found.func, listActivity)

    def test_activity_use_right_function(self):
        found = resolve(self.activity)
        self.assertEqual(found.func, activity)

    def test_register_use_right_function(self):
        found = resolve(self.daftar)
        self.assertEqual(found.func, daftar)

    def test_delete_use_right_function(self):
        found = resolve(self.delete)
        self.assertEqual(found.func, delete)


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("activities:Story6")
        self.listActivity = reverse("activities:listActivity")
        self.activity = reverse("activities:activity")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_GET_listActivity(self):
        response = self.client.get(self.listActivity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listActivity.html')

    def test_GET_activity(self):
        response = self.client.get(self.activity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'activity.html')

    def test_POST_activity(self):
        response = self.client.post(self.activity,
                                    {
                                        'nama_kegiatan': 'masak',
                                        'deskripsi_kegiatan ': "masak bubur"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_activity_invalid(self):
        response = self.client.post(self.activity,
                                    {
                                        'nama_kegiatan': '',
                                        'deskripsi_kegiatan ': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'activity.html')


    def test_GET_delete(self):
        kegiatan = Kegiatan(nama_kegiatan="abc", deskripsi_kegiatan="XYZ")
        kegiatan.save()
        orang = Orang(nama_orang="marketing",
                      kegiatan=Kegiatan.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('activities:delete', args=[orang.pk]))
        self.assertEqual(Orang.objects.count(), 0)
        self.assertEqual(response.status_code, 302)


class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan="abc", deskripsi_kegiatan="ZUZUZU")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/activities/daftar/1/',
                                 data={'nama_orang': 'Marketing'})
        self.assertEqual(response.status_code, 302)

    def test_regist_GET(self):
        response = self.client.get('/activities/daftar/1/')
        self.assertTemplateUsed(response, 'daftar.html')
        self.assertEqual(response.status_code, 200)

    def test_regist_POST_invalid(self):
        response = Client().post('/activities/daftar/1/',
                                 data={'nama': ''})
        self.assertTemplateUsed(response, 'daftar.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(ActivitiesConfig.name, 'activities')
        self.assertEqual(apps.get_app_config('activities').name, 'activities')
