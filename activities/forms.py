from django import forms


class FormKegiatan(forms.Form):
    nama_kegiatan = forms.CharField(
        label="Nama kegiatan",
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control w-60',
                'placeholder':'Enter your activity',
            }
        )
    )

    deskripsi_kegiatan = forms.CharField(
        label="Deskripsi kegiatan",
        max_length=1000,
        widget=forms.Textarea(
            attrs = {
                'class': 'form-control w-60',
                'placeholder':'Describe it!',
            }
        )
    )

class FormOrang(forms.Form):
    nama_orang = forms.CharField(
        label="Nama",
        max_length=64,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder':'Insert your name',
            }
        )
    )